//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Yaroslav Georgievich on 29.07.2018.
//  Copyright © 2018 Yaroslav Georgievich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let newCoffeeMachine = CoffeeMachine.init(containerWater: 500, containerMilk: 300, cupForCoffee: 0)
    @IBOutlet weak var mainDisplay: UILabel!
    
    @IBAction func makeLatte() {
        mainDisplay.text = newCoffeeMachine.makeLatte()
    }
    
    @IBAction func makeAmericano() {
        mainDisplay.text = newCoffeeMachine.makeAmericano()
    }
    
    @IBAction func makeEspresso() {
        mainDisplay.text = newCoffeeMachine.makeEspresso()
    }
    
    @IBAction func addMilk() {
        mainDisplay.text = newCoffeeMachine.addMilk()
    }
    
    @IBAction func addWater() {
        mainDisplay.text = newCoffeeMachine.addWater()
    }
    
    @IBAction func addCups() {
        mainDisplay.text = newCoffeeMachine.addCup()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //let newCoffeeMachine = CoffeeMachine.init(containerWater: 100)
        
    }
    
}

