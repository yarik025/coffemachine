//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Yaroslav Georgievich on 29.07.2018.
//  Copyright © 2018 Yaroslav Georgievich. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    
    var containerWater = 0
    var containerMilk = 0
    var cupForCoffee = 0
    let waterForOneAmericano = 100
    let milkForOneLatte = 50
    let waterForOneEspresso = 50
    
    init(containerWater: Int, containerMilk: Int, cupForCoffee: Int) {
        self.containerWater = containerWater
        self.containerMilk = containerMilk
        self.cupForCoffee = cupForCoffee
    }
    
    func addWater() -> String {
        containerWater = 500
        let waterAdded = "water added"
        return waterAdded
    }
    
    func addMilk() -> String {
        containerMilk = 300
        let milkAdded = "milk added"
        return milkAdded
    }
    
    func addCup() -> String {
        if cupForCoffee == 0 {
            cupForCoffee = 1
            let cupAdded = "cup added"
            return cupAdded
        }
        else {
            let cupPresent = "cup is present"
            return cupPresent
        }
    }
    
    func makeLatte() -> String {
        if containerWater >= 100 && containerMilk >= 50 && cupForCoffee == 1 {
            let displayTakeLatte = "take your Latte"
            containerWater = containerWater - waterForOneAmericano
            containerMilk = containerMilk - milkForOneLatte
            cupForCoffee = cupForCoffee - 1
            return displayTakeLatte
        }
        else if containerWater <= 100 {
            let displayNoWater = "need add water"
            return displayNoWater
        }
        else if containerMilk < 50 {
            let displayNoMilk = "need add milk"
            return displayNoMilk
        }
        else {
            let displayNoCup = "need add cup"
            return displayNoCup
        }
    }
    
    func makeAmericano() -> String {
        if containerWater >= 100 && cupForCoffee == 1{
            let displayTakeAmericano = "take your Americano"
            containerWater = containerWater - waterForOneAmericano
            cupForCoffee = cupForCoffee - 1
            return displayTakeAmericano
        }
        else if containerWater < 100 {
            let displayNoWater = "need add water"
            return displayNoWater
        }
        else {
            let displayNoCup = "need add cup"
            return displayNoCup
        }
    }
    
    func makeEspresso() -> String {
        if containerWater >= 50 && cupForCoffee == 1 {
            let displayTakeEspresso = "take your Espresso"
            containerWater = containerWater - waterForOneEspresso
            cupForCoffee = cupForCoffee - 1
            return displayTakeEspresso
        }
        else if cupForCoffee < 1 {
            let displayNoCup = "need add cup"
            return displayNoCup
        }
        else {
            let displayNoWater = "need add water"
            return displayNoWater
        }
    }
}
